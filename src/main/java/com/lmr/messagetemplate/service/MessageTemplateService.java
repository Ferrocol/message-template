package com.lmr.messagetemplate.service;

import com.lmr.messagetemplate.domain.Company;
import com.lmr.messagetemplate.domain.Guest;
import com.lmr.messagetemplate.domain.Message;
import com.lmr.messagetemplate.domain.MessageContext;
import java.util.List;

public interface MessageTemplateService {
    List<Guest> getAllGuests();
    List<Company> getAllCompanies();
    List<Message> getAllMessages();
    String sendMessage(MessageContext msgCtx);
    void createMessageTemplate(Message message);
}
