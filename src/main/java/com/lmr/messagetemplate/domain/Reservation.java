package com.lmr.messagetemplate.domain;

import java.util.Objects;

public class Reservation {
    private Long roomNumber;
    private Long startTimestamp;
    private Long endTimestamp;

    public Reservation() {
    }

    public Reservation(Long roomNumber, Long startTimestamp, Long endTimestamp) {
        this.roomNumber = roomNumber;
        this.startTimestamp = startTimestamp;
        this.endTimestamp = endTimestamp;
    }

    public Long getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(Long roomNumber) {
        this.roomNumber = roomNumber;
    }

    public Long getStartTimestamp() {
        return startTimestamp;
    }

    public void setStartTimestamp(Long startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    public Long getEndTimestamp() {
        return endTimestamp;
    }

    public void setEndTimestamp(Long endTimestamp) {
        this.endTimestamp = endTimestamp;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.roomNumber);
        hash = 59 * hash + Objects.hashCode(this.startTimestamp);
        hash = 59 * hash + Objects.hashCode(this.endTimestamp);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Reservation other = (Reservation) obj;
        if (!Objects.equals(this.roomNumber, other.roomNumber)) {
            return false;
        }
        if (!Objects.equals(this.startTimestamp, other.startTimestamp)) {
            return false;
        }
        if (!Objects.equals(this.endTimestamp, other.endTimestamp)) {
            return false;
        }
        return true;
    }
}
