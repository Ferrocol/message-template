package com.lmr.messagetemplate.dao;

import com.lmr.messagetemplate.domain.Guest;
import com.lmr.messagetemplate.domain.Reservation;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Repository;

@Repository
public class GuestReservationDaoImpl implements GuestReservationDao {
    private final String JSON_FILE = "Guests.json";
    private List<Guest> guests = new ArrayList<>();

    @Override
    public List<Guest> getAllGuests() {
        guests.clear();
        readGuestsJsonFile();
        return guests;
    }

    private void readGuestsJsonFile() {
        JSONParser parser = new JSONParser();

        try {
            JSONArray guestFile = (JSONArray) parser.parse(
                    new FileReader(JSON_FILE));
            for (Object guestObject : guestFile) {
                JSONObject guestJson = (JSONObject) guestObject;
                Long id = (Long) guestJson.get("id");
                String firstName = (String) guestJson.get("firstName");
                String lastName = (String) guestJson.get("lastName");
                
                JSONObject reservationJson = (JSONObject) 
                        guestJson.get("reservation");
                Long roomNumber = (Long) reservationJson.get("roomNumber");
                Long startTimestamp = (Long) reservationJson.get("startTimestamp");
                Long endTimestamp = (Long) reservationJson.get("endTimestamp");
                
                Reservation reservation = new Reservation(
                        roomNumber, startTimestamp, endTimestamp);
                Guest guest = new Guest(id, firstName, lastName, reservation);
                guests.add(guest);
            }
        } catch (IOException | ParseException e) {
            System.out.println(e.getMessage());
        }
    }
}
