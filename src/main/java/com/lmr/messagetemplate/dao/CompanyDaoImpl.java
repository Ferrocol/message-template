package com.lmr.messagetemplate.dao;

import com.lmr.messagetemplate.domain.Company;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Repository;

@Repository
public class CompanyDaoImpl implements CompanyDao {
    private final String JSON_FILE = "Companies.json";
    private List<Company> companies = new ArrayList<>();

    @Override
    public List<Company> getAllCompanies() {
        companies.clear();
        readCompaniesJsonFile();
        return companies;
    }
    
    private void readCompaniesJsonFile() {
        JSONParser parser = new JSONParser();

        try {
            JSONArray companyFile = (JSONArray) parser.parse(
                    new FileReader(JSON_FILE));
            for (Object companyObject : companyFile) {
                JSONObject companyJson = (JSONObject) companyObject;
                Long id = (Long) companyJson.get("id");
                String company = (String) companyJson.get("company");
                String city = (String) companyJson.get("city");
                String timezone = (String) companyJson.get("timezone");
                
                Company c = new Company(id, company, city, timezone);
                companies.add(c);
            }
        } catch (IOException | ParseException e) {
            System.out.println(e.getMessage());
        }
    }
}
