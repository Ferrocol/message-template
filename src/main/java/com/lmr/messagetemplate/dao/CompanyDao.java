package com.lmr.messagetemplate.dao;

import com.lmr.messagetemplate.domain.Company;
import java.util.List;

public interface CompanyDao {
    List<Company> getAllCompanies();
}
