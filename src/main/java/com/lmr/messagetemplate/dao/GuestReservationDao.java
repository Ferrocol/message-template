package com.lmr.messagetemplate.dao;

import com.lmr.messagetemplate.domain.Guest;
import java.util.List;

public interface GuestReservationDao {
    List<Guest> getAllGuests();
}
