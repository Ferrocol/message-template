package com.lmr.messagetemplate.client;

import org.springframework.stereotype.Component;

@Component
public class TextMessageClient {
    public String sendMessage(String message) {
        // code that sends a text message
        return message;
    }
}
