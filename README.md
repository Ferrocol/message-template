# Message Template Application

This Java web application utilizes JSON templating to automatically populate customizable message templates. The message templates may contain placeholders that can be filled with guest/reservation & company information, as well as a greeting that changes based on the time of day.

## Getting Started

### Prerequisites

* Java JDK 8
* Apache Maven 3.*
* Apache Tomcat 8.5.23

### Installing

Save the compressed project on your local machine and extract to the desired location.

### Deployment

Navigate to the project directory in the terminal (Note: this should be the same directory in which the pom.xml resides).
Run the following command to create a build in the *target* folder:
```
mvn install
```
To ensure any previously-existing build target is removed before a new build, run the following command instead:
```
mvn clean install
```
Run the following command to start the web service.
```
java -jar target/message-template-0.0.1-SNAPSHOT.jar
```
Open your browser to http://localhost:8080 to use the web application's user interface.

## Overview

### Design Decisions

I structured my project with the DAO/repository layer reading from / persisting to JSON files, a service layer to handle the logic associated with building a message from a template, and REST controllers passing data between Java and the front-end.
TextMessageClient.java is a mock file that would (in the real world) be sending out the fully-constructed message. It returns the message itself, so it may be passed up to the front-end via the Response Body and displayed to the user/sender.
For simplicity, the front-end is a single page with forms to construct a message from an existing template, or build a custom template that is added to the list of available pre-loaded messages.
Message placeholders, enclosed by "${" and "}" are matched using RegEx and replaced with greeting, guest / reservation, and company data based on the user's selection, which is passed to Java in a wrapper class -- MessageContext.java.
Rather than constructing a message in the front end, the message and its context are sent to the REST controller in the Request Body so Java can build a fully-populated message. This allows the Message Template Service to exist independently of the front-end, allowing us to replace / forgo the User Interface.

### Language Choice

For the sake of time, I chose to write this in Java as it is the language I'm currently most comfortable in. However, I also chose to build this project with Spring Boot as an opportunity to tinker with something new and learn more about the Spring Framework.
The web page itself was written with HTML & CSS, with dynamic content served using JavaScript and jQuery.

### Verification of Correctness

The UI allows us to easily verify the functionality of the application. If the guest, company, or message data cannot be loaded, error messages will display in the form. Likewise, if a message cannot be submitted or a custom template cannot be saved, a browser alert will be displayed.
Upon successfully sending a message, the UI will display a "message bubble" that contains the resulting message, as if in a text message conversation.
Upon successfully saving a custom message template, the UI displays a pop-up dialog that persists for three seconds -- Materialize's variation on a toaster / pop-up notification.

### Future Features

With more time, I would have written unit tests to completely validate the back-end of the web application.
I also would not rely solely on the front-end for form validations, but given the time constraint this was the quickest and easiest way to ensure a message cannot be constructed without a guest or company selected.
Finally, I would have liked to come up with a more robust solution for the construction of a message template such that the user can still type in their custom message, but is not able to accidentally modify the placeholders (e.g., characters in ${placeholder} mistakenly get deleted and becomes ${place, and can now no longer be found and replaced automatically).

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management
* [Spring](https://spring.io) - Java framework
* [jQuery](http://jquery.com/download/) - Javascript library
* [Materialize] (http://materializecss.com) - A modern responsive front-end framework based on Material Design
* [Bootstrap](http://getbootstrap.com) - Front-end framework

## Authors

* **Leanne Renner** - [GitLab](https://gitlab.com/Ferrocol)